# CSCI544 - Applied Natural Language Processing
# Spring 2015
# Final Project - Author Identification on Twitter
# Getting Timeline Tweets
# Nada Aldarrab		naldarra@usc.edu


import tweepy
import sys
import codecs
import time


# == OAuth Authentication ==
#
# This mode of authentication is the new preferred way
# of authenticating with Twitter.

# The consumer keys can be found on your application's Details
# page located at https://dev.twitter.com/apps (under "OAuth settings")
consumer_key = ''
consumer_secret = ''

# The access tokens can be found on your applications's Details
# page located at https://dev.twitter.com/apps (located
# under "Your access token")
access_token = ''
access_token_secret = ''

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.secure = True
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)
# == End of Authentication ==

# Parameters: (Currently set to get the maximum number of tweets (about 3200 tweets/user))
# Character encoding
encoding = 'utf-8'
# Input file name: users.txt	Input file format: links to usernames
ipfile = 'users.txt'
# Output folder path: users/	Output format: username.txt
opfolder = 'users-utf-8/'
# Number of requests from the Twitter API
requests_count = 16
# Number of tweets per request
tweets_count = 200

# Get timeline tweets
sys.stdout = codecs.getwriter(encoding)(sys.stdout)
counter = 0
with codecs.open(ipfile, 'r', encoding) as ipfile:
	for line in ipfile:
		counter += 1
		user_id = line[:-1].rsplit('/',1)[-1]
		opfile = opfolder + user_id + '.txt'
		with codecs.open(opfile, 'w', encoding) as opfile:
			max_id = None
			for i in range(requests_count):
				my_tweets = api.user_timeline(user_id, max_id=max_id, count=tweets_count)
				for tweet in my_tweets:
				    opfile.write(tweet.text.replace('\n', ' '))
				    opfile.write('\n')
				    max_id = int(tweet.id)-1
		# Keep requests within rate limits
		if counter==10:
			time.sleep(910)
			counter = 0
