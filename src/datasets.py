# CSCI544 - Applied Natural Language Processing
# Spring 2015
# Final Project - Author Identification on Twitter
# Splitting the dataset into training, development, and test sets
# Nada Aldarrab		naldarra@usc.edu


import glob
import sys
import codecs
import random
import math


# Parameters: (Currently set to split the dataset into training(80%), development(10%), and test set(10%))
# Character encoding
encoding = 'utf-8'
# Input folder path
ipfolder = 'users-utf-8/'
# Output folder paths
training_folder = 'training-set/'
dev_folder = 'dev-set/'
test_folder = 'test-set/'
# Training partition size (%)
training_size = 0.8
# Development partition size (%)
dev_size = 0.1
# Test partition size (%)
test_size = 0.1
# ==============================


# Create path name to training data files
tdpath = ipfolder + '/*.txt'
# Make a list of all available training data documents
tdset = glob.glob(tdpath)
# Processing each document
for document in tdset:
	# Get the filename
	filename = document.split('/')[-1]			
	with codecs.open(document, 'r', encoding) as ipfile:
		tweets = ipfile.readlines()
		random.shuffle(tweets)
		# Get set indexes
		train_end = int(math.floor(training_size*len(tweets)))
		dev_end = int(train_end+math.floor(dev_size*len(tweets)))
		# Split the data
		train_data = tweets[:train_end]
		dev_data = tweets[train_end:dev_end]
		test_data = tweets[dev_end:]
		# Create training set
		with codecs.open((training_folder+filename), 'w', encoding) as opfile:
			for tweet in train_data:
				opfile.write(tweet)
		# Create dev set
		with codecs.open((dev_folder+filename), 'w', encoding) as opfile:
			for tweet in dev_data:
				opfile.write(tweet)
		# Create test set
		with codecs.open((test_folder+filename), 'w', encoding) as opfile:
			for tweet in test_data:
				opfile.write(tweet)
				
