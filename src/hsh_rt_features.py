# CSCI544 - Applied Natural Language Processing
# Spring 2015
# Final Project - Author Identification on Twitter
# Extracting hashtag and mention features
# Nada Aldarrab		naldarra@usc.edu


import sys
import random
import codecs

def extractFeatures(ipfile, opfile):
	# Character encoding
	encoding = 'utf-8'
	with codecs.open(ipfile, 'r', encoding) as ipfile:
		with codecs.open(opfile, 'w', encoding) as opfile:
			for line in ipfile:
				tweet = line.split('\t', 1)[1]
				tokens = tweet.split()
				if tokens:
					hsh = []
					mt = []
					for token in tokens:
						if token[0]=='#' and len(token)>1:
							hsh.append(token)
						if token[0]=='@'and len(token)>1:
							mt.append(token)
					hsh += ['nohsh2', 'nohsh1']				
					mt += ['nomt2', 'nomt1']				
					opfile.write(hsh[0] + ',' + hsh[1] + ',' + mt[0] + ',' + mt[1])
					opfile.write('\n')


if __name__ == '__main__':
	# Check if proper arguments have been provided 
	if len(sys.argv) > 2:
		# Get input file path
		ipfile = sys.argv[1] 
		# Get output file path
		opfile = sys.argv[2] 
		extractFeatures(ipfile, opfile)
	else:
		print ('Please specify the path for the input and output files.')


