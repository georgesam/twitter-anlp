import sys
import operator
import json
import re
sys.path.insert(0, "..")
from percepclassify import classifyTest

def getWordShape(word):
	word = re.sub('[a-z]', 'a', word)
	word = re.sub('a+', 'a', word)
	word = re.sub('[A-Z]', 'A', word)
	word = re.sub('A+', 'A', word)
	word = re.sub('[0-9]', '0', word)
	word = re.sub('9+', '9', word)
	return word	

def getFeatures(content , i):

	currWord = content[i]
	#print(currWord)

	suff3 = currWord[-3:]
	suff2 = currWord[-2:]

	wordShape = getWordShape(currWord)
	#print(str(content)+" "+str(i))
	if(len(content) ==1):
		prevWord = "START"
		#nextWordToken = content[i+1].split('/')
		nextWord = "END"
	else:
		if i == 0:
			prevWord = "START"
			nextWord = ''.join(content[i+1])
			#print(nextWord)

		elif (i < (len(content)-1) and i!=0) :
			prevWord = ''.join(content[i-1])

			nextWord = ''.join(content[i+1])
		else:
			prevWord = ''.join(content[i-1])
			nextWord = "END"

	#print("prev:"+prevWord+" curr:"+currWord+" next:"+nextWord+" suff3:"+suff3+" suff2:"+suff2+" wordShape:"+wordShape+"\n")
	return ("prev:"+prevWord+" curr:"+currWord+" next:"+nextWord+" suff3:"+suff3+" suff2:"+suff2+" wordShape:"+wordShape+"\n")


	
def procForDev(content):
	tempList=[]
	for word in content:
		tempList.append(word.split('/')[0])

	return tempList

if __name__=='__main__':

	modelFileName = sys.argv[1]
	with open(modelFileName, 'r', encoding='utf-8') as data_file:    
		data = json.load(data_file)
	totalDocs =0
	correctDocs=0


	for line in sys.stdin:
		outputString =""
		
		content = line.split()
		contentLen = len(content)
		classifiedString=""
		for i in range(0, contentLen):
			#className="xxx"
			# for Dev Set
			#content = procForDev(content)
			
			totalDocs+=1
			outputString = getFeatures(content, i)

			className = classifyTest(outputString, data)
			classifiedString+=content[i]+'/'+className+" "

		
		#print(line)
		print(classifiedString)