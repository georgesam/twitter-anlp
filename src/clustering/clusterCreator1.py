from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
import pickle

count =0
fieldCount =0
classCount =1
devunameList=[]
trainingList = []
unameList=[[]]
userNameList={}
columnList={}
fHash = {}


uList=[]
with open('usersAllBagWords.txt') as myfile:
  

      for line in myfile:
        uName = line.split(' ')[0]
        #
        tempList=uName.split()
        unameList.append(tempList)
        if(uName not in userNameList.keys()):
            userNameList[uName]=classCount
            uList.append(uName)
            classCount+=1
            fHash[uName]={}
            tweetNo=0
            tweetNo=0
            #print(uName)
        
        uHash = fHash.get(uName) 
                
        uHash[tweetNo]=''
        featureVect = ''
        fCount=0
        #tweetHash = uHash.get(tweetNo)

        line=line.strip("\n")   
        featureValList = line.split(' ')
        #print(featureValList)
        
        tweetNo+=1
        
        #print(type(tweetHash))
        for vals in featureValList:
            
            if(fCount == 0):
                fCount+=1 
                continue
            if(vals=='|'):
              vals='PIPE'
            if(vals==':'):
              vals='COLON'
            #print(columnList[fCount]+":"+str(vals)+" ")
            vals=vals.replace(':','COLON')
            vals=vals.replace('|','PIPE')
            featureVect+= str(vals)+" "
     
        
        uHash[tweetNo] = featureVect    
            #tweetHash[feat]=vals
            #print(columnList[fCount]+": "+str(vals))
        fCount+=1
            #print(str(fCount))
        #print(featureVect)
        #outputFilePtr.write(featureVect+"\n")
        #trainingList.append(featureVect)
        #    break
        featureVect=''
        #featureVect.flush()

for u in uList:
    trainingList=[]
    for t in fHash.get(u).keys():
        trainingList.append(fHash.get(u).get(t))

    documents = trainingList

    true_k = 3
    vectorizer = TfidfVectorizer(stop_words='english')
    X = vectorizer.fit_transform(documents)
    model = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1)
    model.fit(X)

    '''print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, model.labels_))
    print("Completeness: %0.3f" % metrics.completeness_score(labels, model.labels_))
    print("V-measure: %0.3f" % metrics.v_measure_score(labels, model.labels_))
    print("Adjusted Rand-Index: %.3f"
          % metrics.adjusted_rand_score(labels, model.labels_))
    print("Silhouette Coefficient: %0.3f"
          % metrics.silhouette_score(X, model.labels_, sample_size=1000))
    '''
    print("\n-----------------\n User:" +u)
    print("\nTop terms per cluster for:")
    order_centroids = model.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names()
    for i in range(true_k):
        print ("Cluster %d:\n" % i)
        for ind in order_centroids[i, :10]:
            print (' %s' % str(terms[ind]).encode('utf-8'))
    

    
#pickle.dump(trainingList, open("scikit20training.p","wb"))


