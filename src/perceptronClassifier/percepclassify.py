import sys
import operator
import json
import codecs

def classifyTest(bagWords, model):
	
	correctDocs = 0
	totalDocs = 0
	wordcount=0
	predictedFeatures={}
	maxLabel = float('-inf')
	
	#print(model['features'])
	#print(bagWords)

	#exit()

	for word in bagWords.split():
		
		wordcount+=1
		#if wordcount ==1:
		#	currClass = word
		#else:

		if word in model['features'].keys():
			#print("xxx")
			if model['features'][word] not in predictedFeatures:
				predictedFeatures[model['features'][word]] = 1
			else:
				predictedFeatures[model['features'][word]] +=1
	#print(predictedFeatures)

	for className in model['weights']:
		predictedLabel = 0
		for feature in predictedFeatures:
			predictedLabel = predictedLabel + (predictedFeatures[feature]* model['weights'][className][feature])
		#print(predictedLabel)
		#exit()

		if predictedLabel > maxLabel:
			maxLabel = predictedLabel
			predictedClass = className
	#print(predictedClass)
	#exit()
	#if predictedClass == currClass:
	#	correctDocs +=1

	return (predictedClass)

	#print(str(correctDocs) + ": " + str(totalDocs))
	#print(correctDocs / totalDocs)

if __name__=='__main__':
	
	modelFileName = sys.argv[1]
	with open(modelFileName, 'r', encoding='utf-8') as data_file:    
	    model = json.load(data_file)
	totalDocs=0
	sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')

	for line in sys.stdin:
		totalDocs+=1
		content=line.split()
		className = classifyTest(line, model)
		print(className)
