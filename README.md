# README #

CSCI544 - Applied Natural Language Processing  
Spring 2015  
Final Project - How Anonymous Can Someone be on Twitter?  
Team # 57  


### About this project ###
Twitter is one of the most popular social networks that experienced rapid growth. With 288 million monthly active users and 500 million Tweets sent per day, Twitter users might assume that they have a certain level of anonymity. Our project investigates whether the author of an anonymous tweet can be identified using stylometry.  


### How to run this code ###
1. Getting Tweets: python3 tltweets.py  
Authentication is required to access the Twitter API (see the first section of code)  
2. Tokenization-Stage I: python3 tokenize.py INPUTFILE OUTPUTFILE  
3. Tokenization-Stage II: python3 tweetnlp-tokenize.py INPUTFOLDER OUTPUTFOLDER  
4. Creating Datasets: python3 datasets.py  
5. Extracting mentions and hashtag features: python3 hsh_rt_features.py INPUTFILE OUTPUTFILE  
5. Extracting word clusters: python3 clustering_features.py INPUTFILE OUTPUTFILE MODELFILE  


### Data ###
Data is available in the data folder. There is a file for each user (filename is the username). Each line of the file contains one tweet. Character encoding is utf-8.  
1. users-utf-8.tar.gz: This is the whole dataset in raw format.  
2. datasets-raw: This folder contains raw data splitted into training, development, and test sets.  
3. datasets-tokenized: This folder contains tokenized tweets splitted into training, development, and test sets.  

  
## Contributors
George Sam - gsam@usc.edu  
Nada Aldarrab - naldarra@usc.edu  
Reihane Bogharti - boghrati@usc.edu  
Vinit Parakh - vparakh@usc.edu  
